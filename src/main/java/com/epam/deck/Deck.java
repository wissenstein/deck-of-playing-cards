package com.epam.deck;

import com.epam.deck.format.Formatter;
import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static com.epam.deck.Card.Suit;
import static com.epam.deck.Card.Rank;

/**
 * Deck of playing cards of French suit, which contains cards 
 * of ranks Ace down to a certain lower rank (commonly 2 or 7).
 * After the deck is created, cards can be taken from it one by one.
 *
 * @author Oleksii_Lupandin
 */
public class Deck {

    private final List<Card> cards;

    private Deck(List<Card> cards) {
        this.cards = cards;
    }

    /**
     * @return new deck containing usual 52 cards of French suit.
     */
    public static Deck of52Cards() {
        return Deck.ofRanksFrom(Rank.TWO);
    }

    /**
     * @return new deck containing usual 32 cards of French suit.
     */
    public static Deck of32Cards() {
        return Deck.ofRanksFrom(Rank.SEVEN);
    }

    /**
     * @param minRank minimum card rank present in the new deck.
     *
     * @return new deck containing cards of ranks
     *      equal to or higher than {@code minRank}
     */
    public static Deck ofRanksFrom(final Rank minRank) {
        List<Card> cards = new LinkedList<>();

        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.getRanksNotLowerThan(minRank)) {
                cards.add(new Card(suit, rank));
            }
        }

        return new Deck(cards);
    }

    /**
     * Returns the first card and removes it from the deck.
     *
     * @return the top card
     */
    public Card next() {
        if (!cards.isEmpty()) {
            return cards.remove(0);
        } else {
            throw new PullFromEmptyDeckException();
        }
    }

    /**
     * Checks if the deck has at least one card.
     *
     * @return {@code true} if the deck has cards, otherwise {@code false}
     */
    public boolean hasNext() {
        return !cards.isEmpty();
    }

    /**
     * Shows all the cards contained by the deck in their order.
     *
     * @return immutable copy of list of all the cards in the deck
     */
    public List<Card> showAllCards() {
        return ImmutableList.copyOf(cards);
    }

    /**
     * Changes the order of the cards in the deck in a pseudo random manner.
     */
    public void shuffle() {
        final Random random = new Random(System.currentTimeMillis());

        final int cardCount = cards.size();
        final Card[] newOrder = new Card[cardCount];
        final Iterator<Card> iterator = cards.iterator();

        // List of free places maps the number of a free place
        // to an index in newOrder array
        final List<Integer> freePlaces = new LinkedList<Integer>() {{
            for (int i = 0; i < cardCount; i++) {
                add(i);
            }
        }};

        while (iterator.hasNext()) {
            final Card card = iterator.next();

            final int newFreePlace = random.nextInt(freePlaces.size());
            final int newIndex = freePlaces.remove(newFreePlace);

            newOrder[newIndex] = card;
        }

        cards.clear();
        cards.addAll(Arrays.asList(newOrder));
    }

    public String format(Formatter formatter) {
        return formatter.format(this);
    }
}
