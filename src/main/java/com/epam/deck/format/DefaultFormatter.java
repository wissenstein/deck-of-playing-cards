package com.epam.deck.format;

import com.epam.deck.Card;
import com.epam.deck.Deck;
import com.google.common.collect.ImmutableMap;
import java.util.Iterator;
import java.util.Map;

import static com.epam.deck.Card.Suit;
import static com.epam.deck.Card.Rank;
import static com.epam.deck.Card.Suit.*;
import static com.epam.deck.Card.Rank.*;

/**
 *
 * @author Oleksij Lupandin
 */
public class DefaultFormatter implements Formatter {

    private static final Map<Suit, String> SUITS
            = ImmutableMap.<Suit, String> builder()
                    .put(SPADES, "S")
                    .put(HEARTS, "H")
                    .put(DIAMONDS, "D")
                    .put(CLUBS, "C")
                    .build();

    private static final Map<Rank, String> RANKS
            = ImmutableMap.<Rank, String> builder()
                    .put(ACE, "A")
                    .put(KING, "K")
                    .put(QUEEN, "Q")
                    .put(JACK, "J")
                    .put(TEN, "10")
                    .put(NINE, "9")
                    .put(EIGHT, "8")
                    .put(SEVEN, "7")
                    .put(SIX, "6")
                    .put(FIVE, "5")
                    .put(FOUR, "4")
                    .put(THREE, "3")
                    .put(TWO, "2")
                    .build();

    @Override
    public String format(Card card) {
        return SUITS.get(card.getSuit()) + RANKS.get(card.getRank());
    }

    @Override
    public String format(Deck deck) {
        final StringBuilder builder = new StringBuilder();
        final Iterator<Card> allCards = deck.showAllCards().iterator();

        if (allCards.hasNext()) {
            builder.append(format(allCards.next()));
        }

        while(allCards.hasNext()) {
            builder.append(" ").append(format(allCards.next()));
        }

        return builder.toString();
    }
}
