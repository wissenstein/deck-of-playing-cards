package com.epam.deck.format;

import com.epam.deck.Card;
import com.epam.deck.Deck;

/**
 * Displays a card as a human-readable string
 * @author Oleksij Lupandin
 */
public interface Formatter {

    public String format(Card card);

    public String format(Deck deck);
}
