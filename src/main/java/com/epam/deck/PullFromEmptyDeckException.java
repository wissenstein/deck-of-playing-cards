package com.epam.deck;

/**
 *
 * @author Oleksii_Lupandin
 */
public class PullFromEmptyDeckException extends RuntimeException {

    /**
     * Creates a new instance of <code>PullFromEmptyDeckException</code> without
     * detail message.
     */
    public PullFromEmptyDeckException() {
        super("Attempt to pull a card from an empty deck.");
    }
}
