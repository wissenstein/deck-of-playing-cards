package com.epam.deck;

import com.epam.deck.format.Formatter;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Oleksii_Lupandin
 */
public class Card {

    public static enum Suit {
        SPADES,
        HEARTS,
        DIAMONDS,
        CLUBS
    }

    public static enum Rank {
        ACE,
        KING,
        QUEEN,
        JACK,
        TEN,
        NINE,
        EIGHT,
        SEVEN,
        SIX,
        FIVE,
        FOUR,
        THREE,
        TWO;

        public static Set<Rank> getRanksNotLowerThan(Rank minRank) {
            final Set<Rank> ranks = new LinkedHashSet<>();

            final int minOrdinal = minRank.ordinal();

            final Rank[] allRanks = values();

            for (int i = 0; i <= minOrdinal; i++) {
                ranks.add(allRanks[i]);
            }

            return ranks;
        }
    }

    private final Suit suit;
    private final Rank rank;

    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    public String format(Formatter formatter) {
        return formatter.format(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.suit);
        hash = 47 * hash + Objects.hashCode(this.rank);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        } else {
            final Card other = (Card) obj;

            return this.suit.equals(other.suit)
                    && (this.rank == other.rank);
        }
    }

    @Override
    public String toString() {
        return "Card{" + "suit=" + suit + ", rank=" + rank + '}';
    }
}
