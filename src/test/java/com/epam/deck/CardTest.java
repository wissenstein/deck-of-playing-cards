/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.deck;

import com.epam.deck.format.DefaultFormatter;
import com.epam.deck.format.Formatter;
import com.google.common.collect.Sets;
import java.util.Set;
import org.junit.Test;

import static org.junit.Assert.*;
import static com.epam.deck.Card.Rank;
import static com.epam.deck.Card.Rank.*;
import static org.hamcrest.CoreMatchers.*;

/**
 *
 * @author Oleksii_Lupandin
 */
public class CardTest {

    @Test
    public void getRanksNotLowerThan() {
        // arrange
        final Rank minRank = JACK;
        final Set<Rank> expectedRanks = Sets.immutableEnumSet(
                JACK,
                QUEEN,
                KING,
                ACE);

        // act
        Set<Rank> ranks = Rank.getRanksNotLowerThan(minRank);

        // assert
        assertThat(ranks, equalTo(expectedRanks));
    }

    @Test
    public void format() {
        // arrange
        final Card jackOfClubs = new Card(Card.Suit.CLUBS, JACK);
        final Formatter formatter = new DefaultFormatter();
        final String expectedOutput = "CJ";

        // act
        String formattedOutput = jackOfClubs.format(formatter);

        // assert
        assertThat(formattedOutput, equalTo(expectedOutput));
    }
}
