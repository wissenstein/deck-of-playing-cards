package com.epam.deck;

import com.epam.deck.format.DefaultFormatter;
import com.epam.deck.format.Formatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static com.epam.deck.Card.Rank;
import static com.epam.deck.Card.Suit;

/**
 *
 * @author Oleksii_Lupandin
 */
public class DeckTest {

    @Test
    public void shouldCreate52CardDeck() {
        // arrange
        final Deck deckOf52Cards = Deck.of52Cards();
        final int expectedCardCount = 52;

        for (int i = 1; i < expectedCardCount; i++) {
            // act
            Card card = deckOf52Cards.next();

            // assert
            assertThat(card, notNullValue());
        }
    }

    @Test
    public void shouldCreateDeckWithArbitraryCardCount() {
        // arrange
        final Card.Rank minRank = Rank.NINE;
        final int expectedCardCount = 24;
        final Deck deckOf24Cards = Deck.ofRanksFrom(minRank);
        final Set<Card> dispencedCards = new HashSet<>(expectedCardCount);

        // act
        while (deckOf24Cards.hasNext()) {
            dispencedCards.add(deckOf24Cards.next());
        }

        // assert
        assertThat(dispencedCards.size(), equalTo(expectedCardCount));
    }

    @Test
    public void shouldShowContainedCards() {
        // arrange
        final Deck deckOf16Cards = Deck.ofRanksFrom(Rank.JACK);

        // act
        final List<Card> cardsInTheirOrder = deckOf16Cards.showAllCards();

        // assert
        assertThat(cardsInTheirOrder, hasItems(
                new Card(Suit.CLUBS, Rank.JACK),
                new Card(Suit.DIAMONDS, Rank.KING),
                new Card(Suit.SPADES, Rank.ACE)));
        assertThat(cardsInTheirOrder, not(hasItems(
                new Card(Suit.CLUBS, Rank.TEN),
                new Card(Suit.HEARTS, Rank.TWO),
                new Card(Suit.SPADES, Rank.EIGHT))));
    }

    @Test
    public void shouldCreate32CardDeck() {
        // arrange
        final int expectedCardCount = 32;
        final Deck deckOf32Cards = Deck.of32Cards();

        // act
        final List<Card> cards = deckOf32Cards.showAllCards();

        // assert
        assertThat(cards.size(), equalTo(expectedCardCount));
    }

    @Test
    public void shouldDispenceCardsInTheirOrder() {
        // arrange
        final Deck deckOf8Cards = Deck.ofRanksFrom(Rank.KING);
        final List<Card> expectedOrder = deckOf8Cards.showAllCards();
        final List<Card> dispencedCards = new ArrayList<>(expectedOrder.size());

        // act
        while (deckOf8Cards.hasNext()) {
            dispencedCards.add(deckOf8Cards.next());
        }

        // assert
        assertThat(dispencedCards, equalTo(expectedOrder));
    }

    @Test(expected = PullFromEmptyDeckException.class)
    public void shouldThrowExceptionWhenTryingToGetCardFromEmptyDeck() {
        // arrange
        final Deck deck = Deck.ofRanksFrom(Rank.QUEEN);
        while (deck.hasNext()) {
            deck.next();
        }
        // deck is empty now

        // act and assert
        deck.next();
    }

    @Test
    public void shouldShuffleCards() {
        // arrange
        final Deck deck = Deck.of52Cards();

        // act
        final List<Card> cardsInOrder1 = deck.showAllCards();
        deck.shuffle();
        final List<Card> cardsInOrder2 = deck.showAllCards();

        // assert
        final Set<Card> cardSet1 = new HashSet<>(cardsInOrder1);
        final Set<Card> cardSet2 = new HashSet<>(cardsInOrder2);
        assertThat(cardSet2, equalTo(cardSet1));
        assertThat(cardsInOrder2, not(equalTo(cardsInOrder1)));
    }

    @Test
    public void shouldShowCardsInTheSameOrderIfNotShuffled() {
        // arrange
        final Deck deck = Deck.of52Cards();

        // act
        final List<Card> cards1 = deck.showAllCards();
        final List<Card> cards2 = deck.showAllCards();

        // assert
        assertThat(cards1, equalTo(cards2));
    }

    @Test
    public void shouldShowItsContentAsAFromattedString() {
        // arrange
        final Deck deck = Deck.ofRanksFrom(Rank.KING);
        final Formatter defaultFormatter = new DefaultFormatter();
        final String expectedOutput = "SA SK HA HK DA DK CA CK";

        // act
        final String output = deck.format(defaultFormatter);

        // assert
        assertThat(output, equalTo(expectedOutput));
    }
}
