package com.epam.deck;

import com.epam.deck.format.DefaultFormatter;
import com.epam.deck.format.Formatter;
import java.util.Scanner;

/**
 * Program which shuffles a deck
 * and outputs cards contained in the deck in their order to stdout.
 *
 * @author Oleksij Lupandin
 */
public class ShuffleCommandLineTest {

    private static final String SHUFFLE = "shuffle";
    private static final String EXIT = "exit";

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        final Deck deck = Deck.of52Cards();

        final Formatter defaultFormatter = new DefaultFormatter();

        System.out.println("The deck contains now cards in this order:");
        System.out.println(deck.format(defaultFormatter) + "\n");
        final String prompt = "Enter \"" + SHUFFLE + "\" to shuffle the deck "
                + "or \"" + EXIT + "\" to exit the program > ";
        System.out.print(prompt);

        String input = scanner.next();
        while(!input.equals(EXIT)) {
            if (input.equals(SHUFFLE)) {
                deck.shuffle();
            }
            System.out.println("\nNow the order is:");
            System.out.println(deck.format(defaultFormatter) + "\n");
            System.out.print(prompt);
            input = scanner.next();
        }

        System.out.println("\nBye!");
    }
}
